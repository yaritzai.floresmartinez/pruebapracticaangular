/**
 * Copyright: --
 * Descripción: Servicio para grupos
 * Autor: María Yaritzai Flores Martínez
 * F. Creación: 12/12/2022
 * F. Modificación: -
*/

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GroupsService {

  //#region Variables
  private readonly apiController: string = 'https://6edeayi7ch.execute-api.us-east-1.amazonaws.com/v1/examen/';
  //#endregion

  //#region Constructor
  constructor( private http : HttpClient) { }
  //#endregion

  //#region Métodos
  /**
    * Método para obtener una lista de grupos
  */
  getGroups( ) {
    return this.http.get(`${this.apiController}groups/yaritzai_flores`);
  }
  
  /**
    * Método para obtener una lista de empleados por grupo
    * @param { String } id - Id
   */
  getEmployeesByGroup( id: number) {
    return this.http.get(`${this.apiController}employees/yaritzai_flores/getByGroup?id=${id}`);
  }
  //#endregion
}
