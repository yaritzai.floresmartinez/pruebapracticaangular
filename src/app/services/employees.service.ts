/**
 * Copyright: --
 * Descripción: Servicio para empleados
 * Autor: María Yaritzai Flores Martínez
 * F. Creación: 11/12/2022
 * F. Modificación: -
*/

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
// Interfaces
import { NewEmployee } from '../interfaces/new-employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeesService {

  //#region Variables
  private readonly apiController: string = 'https://6edeayi7ch.execute-api.us-east-1.amazonaws.com/v1/examen/employees/yaritzai_flores';
  //#endregion
  
    //#region Constructor
    constructor( private http : HttpClient) { }
    //#endregion
  
    //#region Métodos
    /**
      * Método para obtener una lista de empleados
    */
    getEmployees( ) {
      return this.http.get(`${this.apiController}`);
    }

    
    /**
      * Método para registrar a un empleado
    */
    postEmployees(data: NewEmployee): Observable<any> {
      return this.http.post(`${this.apiController}`, data);
    }

    //#endregion
  
}
