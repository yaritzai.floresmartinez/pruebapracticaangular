import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Rutas
import { appRounting } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { EmployeesComponent } from './pages/employees/employees.component';

// Para componentes
import { MatTableModule } from '@angular/material/table';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatPaginatorModule } from '@angular/material/paginator';
import { IvyCarouselModule } from 'angular-responsive-carousel';

import { DragDropModule } from '@angular/cdk/drag-drop';

// Servicios
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { EmployeesService } from '../app/services/employees.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GroupsComponent } from './pages/groups/groups.component';
import { RegisterEmployeeComponent } from './pages/register-employee/register-employee.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    EmployeesComponent,
    GroupsComponent,
    RegisterEmployeeComponent
  ],
  imports: [
    BrowserModule,
    IvyCarouselModule,
    appRounting,
    HttpClientModule,
    BrowserAnimationsModule,
    MatTableModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatPaginatorModule,
    DragDropModule
  ],
  providers: [
    EmployeesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
