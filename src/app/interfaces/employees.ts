/**
 * Copyright: --
 * Descripción: Interfaz para registrar empleados
 * Autor: María Yaritzai Flores Martínez
 * F. Creación: 12/12/2022
 * F. Modificación: -
*/

/**
 * Interfaz de Empleado
*/

export interface Employees {
    id: number;
    name: string;
    lastName: string;
    birthday: number;
}