/**
 * Copyright: --
 * Descripción: Interfaz para registrar empleados
 * Autor: María Yaritzai Flores Martínez
 * F. Creación: 12/12/2022
 * F. Modificación: -
*/

/**
 * Interfaz para nuevo empleado
*/
export interface NewEmployee {
    name: string;
    last_name: string;
    birthday: string;
}