/**
 * Copyright: --
 * Descripción: Componente de inicio
 * Autor: María Yaritzai Flores Martínez
 * F. Creación: 11/12/2022
 * F. Modificación: -
*/
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
