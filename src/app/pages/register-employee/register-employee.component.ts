/**
 * Copyright: --
 * Descripción: Componente para registrar empleados
 * Autor: María Yaritzai Flores Martínez
 * F. Creación: 12/12/2022
 * F. Modificación: -
*/
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
// Services
import { EmployeesService } from '../../services/employees.service';
// Interfaces
import { NewEmployee } from '../../interfaces/new-employee';

@Component({
  selector: 'app-register-employee',
  templateUrl: './register-employee.component.html',
  styleUrls: ['./register-employee.component.css']
})
export class RegisterEmployeeComponent implements OnInit {

  selected: Date | null;

  //#region Varibles
  form: FormGroup;
  dataEmployee: NewEmployee;
  //#endregion

  //#region Settings
  //#endregion

  constructor( private fB: FormBuilder, public employeesService: EmployeesService) {
    // Crear el formulario
    this.createForm();
  }

  //#region Methods
  ngOnInit(): void {
  }

  // Método que crea el formulario
  createForm(){
    this.form = this.fB.group({
      name: ['', [Validators.required, Validators.maxLength(30)]],
      lastName: ['', [Validators.required,  Validators.maxLength(30)]],
      date: ['', Validators.required]
    });
  }

  // Método que recupera la informacion del formulario
  save() {
    // Validando que exista información
    if ( this.form.invalid ) {
      return Object.values(this.form.controls).forEach( control => {
      control.markAsTouched();
     });
     } else {
      const regex = /-/gi;
      // Se ontiene la información del formulario y se adapta para post
      this.dataEmployee = {
        name: this.form.value.name,
        last_name: this.form.value.lastName,
        birthday: this.form.value.date.replace(regex, '/'),
      }

      // Se envia la información mediante POST
      this.employeesService.postEmployees(this.dataEmployee).subscribe(
        (info) => {
          var resultado = window.confirm('Se ha registrado!');
          if (resultado === true) {
              this.form.reset();
          }
        },
        (err) => console.error(err),
        // tslint:disable-next-line: no-console
        () => console.log('done creating employee'),
      );
  
     }
  }

  // Procesar validaciones
  get nameNoValid() {
  return this.form.get('name').invalid && this.form.get('name').touched
  }
  get lastNameNoValid() {
  return this.form.get('lastName').invalid && this.form.get('lastName').touched
  }
  get dateNoValid() {
  return this.form.get('date').invalid && this.form.get('date').touched
  }
  //#endregion

}
