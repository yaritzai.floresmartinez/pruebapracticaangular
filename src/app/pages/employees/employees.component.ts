/**
 * Copyright: --
 * Descripción: Componente para consultar empleados
 * Autor: María Yaritzai Flores Martínez
 * F. Creación: 11/12/2022
 * F. Modificación: -
*/
import { Component, ViewChild, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
// Services
import { EmployeesService } from '../../services/employees.service';
// Interfaces
import { Employees } from '../../interfaces/employees';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;

  //#region Varibles
  employes: Employees[];
  dataSource;
  //#endregion

  //#region Settings
  displayedColumns: string[] = ['id','name','lastName','birthday'];
  //#endregion
  constructor( public EmployeesService: EmployeesService ) { }

  //#region Methods
  ngOnInit(): void {
    // Consulta empleados
    this.EmployeesService.getEmployees()
    .subscribe((employees: any) => {
      this.employes = employees.data.employees;
      this.dataSource = new MatTableDataSource(this.employes);
      this.dataSource.paginator = this.paginator;
    });
  }

  // Método que aplica filtros
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  //#endregion
}
