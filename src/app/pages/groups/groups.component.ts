/**
 * Copyright: --
 * Descripción: Componente para consultar grupos
 * Autor: María Yaritzai Flores Martínez
 * F. Creación: 12/12/2022
 * F. Modificación: -
*/
import { Component, OnInit } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem, CdkDrag } from '@angular/cdk/drag-drop';
// Services
import { GroupsService } from '../../services/groups.service';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css']
})
export class GroupsComponent implements OnInit {

  //#region Variables
  groups = [];
  employees = [];
  //#endregion

  //#region Constructor
  constructor( public groupsService: GroupsService ) { }
  //#endregion

  //#region Métodos
  ngOnInit(): void {
    // Consultamos los grupos
    this.groupsService.getGroups()
    .subscribe((groups: any) => {
      this.groups = groups.data.groups;
    });
  }
  
  // Evento que permite mover las tarjetas
  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }
  }

  // Método que consulta empleados por grupo
  employeesByGroup(id: number): void {
    // Consultamos los empleados por id de grupo
    this.groupsService.getEmployeesByGroup(id)
    .subscribe((employees: any) => {
      console.log('employees: ', employees);
      //this.groups = employees.data.groups;
    });
  }

  //#endregion

}