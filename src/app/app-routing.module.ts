import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { EmployeesComponent } from './pages/employees/employees.component';
import { RegisterEmployeeComponent } from './pages/register-employee/register-employee.component';
import { GroupsComponent } from './pages/groups/groups.component';

export const routes: Routes = [
  { path: 'home',component: HomeComponent },
  { path: 'employees',component: EmployeesComponent },
  { path: 'registerEmployee',component: RegisterEmployeeComponent },
  { path: 'groups',component: GroupsComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', redirectTo: 'home' },
];

const config: ExtraOptions = {
  useHash: true,
};

export const appRounting = RouterModule.forRoot(routes);
